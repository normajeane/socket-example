# TCP/IP Example

![](./figure/socket.png)

## Example 1
- Server : Perception PC (Python on the Linux)
  - Detect to Aruco marker
  - Send the pose of the marker
---
- Client : Robot PC (C++ with MFC on the Window)
  - Recieve the pose of the marker

---


## Example 2
- Server, Client : Same PC (Python on the Linux)

![](./figure/demo.gif)
