#pragma once
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <WinSock2.h>
#include <string>

typedef class TCPCLIENT{
public:
	static UINT recvThread(LPVOID p);
	bool connectRGBD(CString ipAddress, uint16_t portNumber); // port range : 0 ~ 65535
	void error_handling(char* message);
	bool disconnectTCP(bool isRobot);

	WSADATA wsaData;
	SOCKET rgbdSocket;
	SOCKADDR_IN rgbdAddr;
	float xData, yData, zData, rxData, ryData, rzData;
		
}tcpClient;