/*
* tcpClient.cpp
* 
* Connect to Universal Robot, Realsense camera
*
*  Created on: 2018. 11. 14.
*      Author: shin
*/
#include "pch.h"
#include "tcpClient.h"
#include <iostream>
using namespace std;

// rgbd camera to object pose
UINT tcpClient::recvThread(LPVOID p){

	tcpClient *t = (tcpClient*)p;
	unsigned char readBuff[1024];
	CString strObject;
	CString strTx;    CString strTy;    CString strTz;
	int n_parse, num_param;
	int len; 

	for (;;)
	{
		n_parse = 0;
		len = recv(t->rgbdSocket, (char*)readBuff, sizeof(readBuff), 0); // return -1: error
		strObject = readBuff;
		//strCntObject = strObject.Mid(n, 2);		n = n + 2;
		//Totalcount = _ttoi(strCntObject); // 전체 물체 수에 따라 반복문 횟수 변경. CString -> int : _ttoi 사용	
		num_param = 2;

		for (int i = 0; i < num_param; i++)
		{
			strTx = strObject.Mid(n_parse, 7); n_parse = n_parse + 7;
			strTy = strObject.Mid(n_parse, 7); n_parse = n_parse + 7;
			//strTz = strObject.Mid(n_parse, 6); n_parse = n_parse + 6;

			tx = _tstof(strTx);
			ty = _tstof(strTy);
			cout << tx << endl;
			cout << ty << endl;
			//tz = _tstof(strTz) * 0.01;
		}
	}
}
// disconnect
bool tcpClient::disconnectTCP(bool isRobot){
	closesocket(rgbdSocket);
	std::cout << "Disconnect to RGB-D TCP/IP." << std::endl;
	WSACleanup();
	return 0;
}
// connect to RGB-D camera
bool tcpClient::connectRGBD(CString ipAddress, uint16_t portNumber)
{
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		error_handling("WSAStartup() error");

	//  Create client socket //
	rgbdSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (rgbdSocket == INVALID_SOCKET)
		error_handling("socket() error");

	// Set the server information //
	memset(&rgbdAddr, 0, sizeof(rgbdAddr));
	rgbdAddr.sin_family = AF_INET;
	
	
	//rgbdAddr.sin_addr.s_addr = inet_addr((char*)(LPCTSTR)ipAddress);
	rgbdAddr.sin_addr.s_addr = inet_addr("192.168.0.36");

	rgbdAddr.sin_port = htons(portNumber);

	// allocate address in socket. //
	if (connect(rgbdSocket, (SOCKADDR*)&rgbdAddr, sizeof(rgbdAddr)) == SOCKET_ERROR)
	{
		error_handling("connect() error");
		return false;
	}
	printf("Success TCP/IP connection.\n");
	AfxBeginThread(recvThread, this);
	return 0;
}

// error code
void tcpClient::error_handling(char * message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	//AfxMessageBox(message);
	//exit(1);
}