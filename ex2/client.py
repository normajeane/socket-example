#!/usr/bin/env python3	
import time
import socket	

HOST = '127.0.0.1'  	                                                # LOCAL IP
PORT = 9999       	
total_message = 10

if __name__ == "__main__":
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	# 소켓 생성
    client_socket.connect((HOST, PORT))	

    for i in range(1, total_message):
        msg = 'Hello, Server! , ' + str(i)
        data = msg.encode()	                                                # 바이너리(byte)형식으로 encode
        length = len(data)
        client_socket.sendall(length.to_bytes(4, byteorder="little"))         # 데이터 길이 전송(little endian)
        client_socket.sendall(data);	                                        # 데이터 전송

        data = client_socket.recv(4);	                                        # server로부터 전송받을 데이터 길이 수신	
        length = int.from_bytes(data, "little");	                        # 데이터 길이는 little endian으로 int로 변환	
        data = client_socket.recv(length);	                                # 데이터 수신
        msg = data.decode();	
        print('Received from server: ', msg);	
        time.sleep(1)

    client_socket.close();
