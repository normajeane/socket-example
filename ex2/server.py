#!/usr/bin/env python3
import socket, threading	

SERVER_IP = ''                                                                  # None 대신 '' 사용
PORT = 9999

def binder(client_socket, addr):	
    print('Connected by', addr)
    try:	
        while True:	
            data = client_socket.recv(4)	                                        # 연결된 소켓으로부터 데이터 수신
            length = int.from_bytes(data, "little") 	                                # client로부터 데이터 크기를 little endian으로 수신. byte에서 int형식으로 변환
            data = client_socket.recv(length) 	
            msg = data.decode() 	                                                # 수신된 데이터를 string 형식으로 decode
            print('Received from', addr, msg) 	
            msg = "Hi, Client!" 
            data = msg.encode() 	                                                # 바이너리(byte)형식으로 변환	
            length = len(data) 	
            client_socket.sendall(length.to_bytes(4, byteorder="little")) 	        # 데이터 사이즈를 little 엔디언 형식으로 byte로 변환한 다음 전송	
            client_socket.sendall(data) 	                                        # echo 데이터를 전송
    except:	                                                                # 접속이 끊길 경우
        print("except : " , addr) 	
    finally:	
        client_socket.close()	

if __name__ == "__main__":
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	        # 소켓 생성
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)	        # 소켓 레벨과 데이터 형태를 설정
    server_socket.bind((SERVER_IP, PORT))
    server_socket.listen()
    try:	
        while True:	                                                                # Multi-client 가능
            client_socket, addr = server_socket.accept()	
            th = threading.Thread(target=binder, args = (client_socket,addr))	        # Thread 생성(binder fn) 후 새로운 client 접속 대기
            th.start()	
    except:	
        print("server")	
    finally:	
        server_socket.close()
